import * as cheerio from 'cheerio'
import * as rp from 'request-promise'

// harry type defs
export interface IHarry {
    selector: string
    attributes: string[]
  }

export interface IHarrySelectorResult {
    attribute: string
    value: string
  }

export interface IHarryResult {
    selector: string
    result: IHarrySelectorResult[]
  }

// return promise based array via async await
export const harry = async (linkToArticle: string, params: IHarry[]): Promise<IHarryResult[]> => {
  // load the article
  const articleText = await rp(linkToArticle)

  const $ = cheerio.load(articleText)
  // generate a results list for each selector
  const selectorResults: IHarryResult[] = []
  params.forEach((param) => {
    $('body')
      .find(param.selector)
      .each((_, element) => {
        // find all the attrs for this selected elemen
        const attributesFound: IHarrySelectorResult[] = []
        param.attributes.forEach((attr) => {
            if (attr  === 'text') {
            attributesFound.push({
              attribute: attr,
              value: $(element).text(),
            })
          } else {
            attributesFound.push({
              attribute: attr,
              value: $(element).attr(attr),
            })
          }
      })

        selectorResults.push({
            result: attributesFound,
            selector: param.selector,
      })
    })
  })

  return selectorResults
}
