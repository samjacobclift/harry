# Webscrape analyser library
Allows a user to extract out all the relevant content from a webpage based on links

# Installing
    npm i harry-js


# Example Typescript Usage
```
import { harry, IHarry } from 'harry-js'

const selectors: IHarry[] = [{
    attributes: ['src'],
    selector: 'img',
}]

const run = async () => {
    const res = await harry('https://www.bbc.co.uk/news/technology-44779998', selectors)
    console.log('found res ', res.length)
    res.forEach((r) => {
        r.result.forEach((v) => {
            console.log(v.attribute, ' ', v.value)
        })
    })
}
run();
```


# Development Set Up
Clone this repo and run
    npm i

# Building
    npm run build

