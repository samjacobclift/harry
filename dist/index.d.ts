export interface IHarry {
    selector: string;
    attributes: string[];
}
export interface IHarrySelectorResult {
    attribute: string;
    value: string;
}
export interface IHarryResult {
    selector: string;
    result: IHarrySelectorResult[];
}
export declare const harry: (linkToArticle: string, params: IHarry[]) => Promise<IHarryResult[]>;
