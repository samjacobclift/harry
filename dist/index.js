"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const cheerio = require("cheerio");
const rp = require("request-promise");
// return promise based array via async await
exports.harry = (linkToArticle, params) => __awaiter(this, void 0, void 0, function* () {
    // load the article
    const articleText = yield rp(linkToArticle);
    const $ = cheerio.load(articleText);
    // generate a results list for each selector
    const selectorResults = [];
    params.forEach((param) => {
        $('body')
            .find(param.selector)
            .each((_, element) => {
            // find all the attrs for this selected elemen
            const attributesFound = [];
            param.attributes.forEach((attr) => {
                if (attr === 'text') {
                    attributesFound.push({
                        attribute: attr,
                        value: $(element).text(),
                    });
                }
                else {
                    attributesFound.push({
                        attribute: attr,
                        value: $(element).attr(attr),
                    });
                }
            });
            selectorResults.push({
                result: attributesFound,
                selector: param.selector,
            });
        });
    });
    return selectorResults;
});
